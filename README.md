
# Démineur

Le but de ce projet est d'être un support de discussion afin de découvrir un développeur.
Le but n'est pas d'avoir un projet complètement fonctionnel, mais de faire travailler le candidat sur des problèmes algorithmiques, et de pouvoir discuter avec lui des décisions qu'il prend : le niveau d'avancement en fin d'entretien n'a donc pas d'importance.

Le projet est découpé en une suite de tâches, amenant à terme à un démineur complet.

Après avoir cloné le projet, vous pouvez vous rendre dans le fichier `energic-entretiens/src/components/minesweeper/minesweeper.js` pour commencer à développer.

**T1** : Afficher une case, de dimensions 30*30px, avec des bordures noires

**T2** : Afficher un quadrillage de 20x20 cases

**T3** : Donner la capacité à votre programme d'afficher un quadrillage de taille paramétrable.

**T4** : Donnez un attribut booleen `hasBomb` aux cases, et définissez le à `true` pour 10% des cases

**T5** : Afficher dans chaque case le nombre de bombes présentes dans son voisinage (cf image)

![Voisinage de Moore](https://upload.wikimedia.org/wikipedia/commons/8/8e/Nbhd_moore_1.png)

**T6** : Pour chaque case, mettre un booleen `clicked`, initialisé à `false` au démarrage.
Un clic sur une case fait passer le booleen à `true` (si le booleen est déjà `true`, il ne se passe rien).
Après un clic sur une case où clicked est `false`, afficher dans celle-ci
* "B" lorsqu'elle est contient une bombe
* Laisser vide si elle n'a pas de bombes dans ses voisins
* Afficher le nombre de bombes voisines sinon.

**T7** : Lors d'un clic sur une case non cliquée qui n'a pas de bombe voisine, déclencher automatiquement un clic sur ses cases voisines (et donc, si une des voisines n'a pas de bombe voisine non plus, elle déclenche le même effet sur ses voisines).

**T8** : Après chaque clic, vérifier le résultat :
* Si l'utilisateur a cliqué sur une bombe, afficher "Perdu" dans une alerte
* Si l'utilisateur a cliqué sur toutes les cases qui ne sont pas des bombes, afficher "Gagné" dans une alerte