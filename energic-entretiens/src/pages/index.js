import React from "react";

import Layout from "../components/layout";
import Minesweeper from "../components/minesweeper/minesweeper";

const IndexPage = () => (
  <Layout>
    <h2>Bienvenue au test de capacités Energic !</h2>
    <Minesweeper />
  </Layout>
);

export default IndexPage
